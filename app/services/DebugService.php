<?php

declare(strict_types=1);

namespace App\Services;

use MilanKyncl\Debugbar\PhalconDebugbar;
use Phalcon\DI;

/**
 * Class DebugService
 * @author Andreas Kugler <guni3.01@gmail.com>
 */
class DebugService
{
    /**
     * Add or remove the debugbar
     */
    public function debugBar()
    {
        $di        = DI::getDefault();
        $config    = $di->get('config');
        $debugMode = $this->prepairDebugMode($config->application->debugMode);

        if ($config->application->debugBar === true) {
            $phalconDebugBar = new PhalconDebugbar();
            $phalconDebugBar->setDebugMode($debugMode);
            $phalconDebugBar->listen();
        } else {
            $di->remove('debug');
        }
    }

    /**
     * Unfortunately, debugmode can be a Boolean value or the contents of an array. This is returned by dotenv as a string.
     * Boolean values are cast and the array is separated by commas.
     *
     * @param string $debugMode
     *
     * @return array|bool
     */
    private function prepairDebugMode($debugMode)
    {
        switch ($debugMode) {
            case 'true':
                return true;
            case 'false':
                return false;
            default:
                $ipAddresses = explode(',', $debugMode);
                if (empty($ipAddresses)) {
                    return false;
                }
                return $ipAddresses;
        }
    }
}
