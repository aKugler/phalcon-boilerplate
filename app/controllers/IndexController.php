<?php

declare(strict_types=1);

namespace App\Controllers;

/**
 * Class IndexController
 * @package App\Controllers
 */
class IndexController extends ControllerBase
{
    public function indexAction()
    {
    }
}
