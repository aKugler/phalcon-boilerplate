<?php

declare(strict_types=1);

namespace App\Controllers;

use Phalcon\Mvc\Controller;

/**
 * Class ControllerBase
 * @package App\Controllers
 */
class ControllerBase extends Controller
{
}
