<?php

declare(strict_types=1);

namespace App\Controllers;

use Phalcon\Mvc\Controller;

/**
 * Class ErrorController
 * @package App\Controllers
 * @author  Andreas Kugler <guni3.01@gmail.com>
 */
class ErrorController extends Controller
{
    /**
     * handle 404-error-page
     */
    public function notFoundAction()
    {
        $this->response->setStatusCode(404, 'Not Found');
    }

    /**
     * handle 500-error-page
     */
    public function uncaughtExceptionAction()
    {
        $this->response->setStatusCode(500, 'Internal Server Error');
    }
}
