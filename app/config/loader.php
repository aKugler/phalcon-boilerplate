<?php

declare(strict_types=1);

use Phalcon\Loader;

/** @var Loader $loader */
$loader = new Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 * @var $config
 */
$loader->registerNamespaces(
    [
        'App\Cache'                 => $config->application->cacheDir,
        'App\Controllers'           => $config->application->controllersDir,
        'App\Models'                => $config->application->modelsDir,
        'App\Migrations'            => $config->application->migrationsDir,
        'App\Forms'                 => $config->application->formsDir,
        'App\Library'               => $config->application->libraryDir,
        'App\Plugin'                => $config->application->pluginsDir,
        'App\Views'                 => $config->application->viewsDir,
        'App\Service'               => $config->application->serviceDir,
        'App\Services\DataHandlers' => $config->application->dataHandlerDir,
        'App\Tasks'                 => $config->application->tasksDir,
    ]
);

$loader->register();
