<?php

declare(strict_types=1);

use App\Services\DebugService;
use Phalcon\Di\FactoryDefault\Cli as CliDi;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\{Php as PhpEngine, Volt as VoltEngine};
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt'  => function ($view) {
            $config = $this->getConfig();

            switch ($config->application->env) {
                case 'dev':
                    $compileAlways = true;
                    break;
                default:
                    $compileAlways = false;
            }

            $volt = new VoltEngine($view, $this);

            $volt->setOptions(
                [
                    'compiledPath'      => $config->application->cacheDir,
                    'compiledSeparator' => '_',
                    'compileAlways'     => $compileAlways,
                ]
            );

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class  = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash(
        [
            'error'   => 'alert alert-danger',
            'success' => 'alert alert-success',
            'notice'  => 'alert alert-info',
            'warning' => 'alert alert-warning'
        ]
    );
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Start the DebugService and show a debugbar (if enabled in .env)
 */
$di->setShared('debug', function () {
    $debug = new DebugService();
    $debug->debugBar();
    return;
});

/**
 * handle 404 and 500
 */
if (!$di instanceof CliDi) {
    $di->setShared(
        'dispatcher',
        function () use ($di) {
            $eventsManager = $di->getShared('eventsManager');
            $eventsManager->attach(
                'dispatch:beforeException',
                function ($event, $dispatcher, $exception) {
                    /** @var Exception $exception */
                    /** @var Dispatcher $dispatcher */
                    switch ($exception->getCode()) {
                        case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                        case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                            $dispatcher->forward(
                                [
                                    'controller' => 'error',
                                    'action'     => 'notFound',
                                ]
                            );
                            return false;
                            break;
                        default:
                            $dispatcher->forward(
                                [
                                    'controller' => 'error',
                                    'action'     => 'uncaughtException',
                                ]
                            );
                            return false;
                            break;
                    }
                }
            );
            $dispatcher = new Dispatcher();
            $dispatcher->setEventsManager($eventsManager);
            return $dispatcher;
        }
    );
}
