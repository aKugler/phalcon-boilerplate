<?php

declare(strict_types=1);

use Phalcon\Config;
use Symfony\Component\Dotenv\Dotenv;

/*
 * We use this constants for the dev-tools (e.g. phalcon model ...)
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');
require_once BASE_PATH . '/vendor/autoload.php';

$dotEnv = new Dotenv();
$dotEnv->load(BASE_PATH . '/.env');

return new Config(
    [
        'database'    => [
            'adapter'  => getenv('DATABASE_ADAPTER'),
            'host'     => getenv('DATABASE_HOST'),
            'username' => getenv('DATABASE_USER'),
            'password' => getenv('DATABASE_PASSWORD'),
            'dbname'   => getenv('DATABASE_NAME'),
            'charset'  => getenv('DATABASE_CHARSET'),
        ],
        'application' => [
            'appDir'         => APP_PATH . '/',
            'controllersDir' => APP_PATH . '/controllers/',
            'formsDir'       => APP_PATH . '/forms/',
            'libraryDir'     => APP_PATH . '/library/',
            'migrationsDir'  => APP_PATH . '/migrations/',
            'modelsDir'      => APP_PATH . '/models/',
            'pluginsDir'     => APP_PATH . '/plugins/',
            'serviceDir'     => APP_PATH . '/services/',
            'dataHandlerDir' => APP_PATH . '/services/dataHandlers/',
            'viewsDir'       => APP_PATH . '/views/',
            'tasksDir'       => APP_PATH . '/tasks/',
            'cacheDir'       => BASE_PATH . '/cache/',

            // This allows the baseUri to be understand project paths that are not in the root directory
            // of the webpspace.  This will break if the public/index.php entry point is moved or
            // possibly if the web server rewrite rules are changed. This can also be set to a static path.
            'baseUri'        => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
            'env'            => getenv('APP_ENV') ?? 'live',
            'debugBar'       => getenv('DEBUG_BAR') === 'true' ? true : false,
            'debugMode'      => getenv('DEBUG_MODE'),
        ]
    ]
);
