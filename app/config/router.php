<?php

declare(strict_types=1);

use Phalcon\Mvc\{Micro, Router};

/** @var Micro $di */
/** @var Router $router */
$router = $di->getRouter();
$router->setDefaultNamespace(
    'App\Controllers'
);

// Define your routes here

$router->handle();
