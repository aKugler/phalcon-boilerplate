# Boilerplate for Phalcon v3

Template for a Phalcon-based Application. Includes namespaces, dotenv, console and a debug toolbar.

#### Do following steps:

* git clone git@github.com:aKugler/phalcon-boilerplate.git project-name
* cd project-name
* git remote remove origin
* git remote add origin git-url
* git push origin master
* cp .env.dist .env
* Fill the file with your values.
* composer install
* Build your own Controllers, etc
* enjoy

#### How to use the console:
```
php bin/console {task} {acton} {param1} {param2} ...

# e.g. php bin/console feed create google promotion
# @see https://docs.phalcon.io/3.4/en/application-cli#processing-action-parameters
```